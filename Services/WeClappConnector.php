<?php

namespace HrWeClappConnector\Services;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use HrWeClappConnector\Libs\ContentTypes;
use HrWeClappConnector\Libs\WeClappApi;
use JsonException;
use Shopware\Components\HttpClient\GuzzleFactory;
use GuzzleHttp\Exception\RequestException;
use Shopware\Components\Logger;
use Symfony\Component\Console\Style\SymfonyStyle;

class WeClappConnector
{
    /**
     * @var Client|ClientInterface
     */
    private $gClient;

    /**
     * @var string[]
     */
    private array $options;

    /**
     * @var array
     */
    private $configs;

    /**
     * @var Connection
     */
    private Connection $connection;

    /**
     * @var Logger
     */
    private Logger $pluginLogger;

    /**
     * WeClappConnector constructor.
     *
     * @param GuzzleFactory $factory
     * @param Connection    $connection
     * @param Logger        $pluginLogger
     * @param               $configs
     *
     */
    public function __construct(GuzzleFactory $factory, Connection $connection, Logger $pluginLogger, $configs)
    {
        $this->connection   = $connection;
        $this->pluginLogger = $pluginLogger;
        $this->configs      = $configs;
        $this->gClient      = $factory->createClient(
            [
                'base_uri' => "https://" . $this->configs['hrWeClappTenant'] . "." . WeClappApi::API_URL,
                'timeout'  => 10,
                'headers'  => [
                    'Content-Type'  => ContentTypes::JSON,
                    'Accept'        => ContentTypes::JSON,
                    'Authorization' => [
                        'Basic ' . base64_encode(
                            WeClappApi::API_USER . ':' . $this->configs['hrWeClappAPIToken']
                        ),
                    ],
                ],
            ]
        );
    }

    /**
     * @param $data
     *
     * @throws JsonException
     */
    private function setBodyOptions($data): void
    {
        $this->options['body'] = json_encode($data, JSON_THROW_ON_ERROR);
    }

    /**
     * @param string $path
     * @param        $params
     */
    private function setQueryOptions(string $path = "", $params = null): void
    {
        $queryStringParams = "";

        switch ($path) {
            case WeClappApi::ORDER:
            case WeClappApi::CUSTOMER:
                if (!empty($params)) {
                    if (is_string($params)) {
                        $queryStringParams = http_build_query(['customerNumber-eq' => $params]);
                    } elseif (is_array($params)) {
                        $queryStringParams = http_build_query(
                            [
                                'customerNumber-eq'        => $params['c'],
                                'orderNumberAtCustomer-eq' => $params['o'],
                            ]
                        );
                    }
                }
                break;
            case WeClappApi::COMMENT:
                $queryStringParams = http_build_query(
                    [
                        'entityName' => 'salesOrder',
                        'entityId'   => $params,
                    ]
                );
                break;
            case WeClappApi::STOCK:
                if (is_array($params)) {
                    $params = '"' . implode('","', $params) . '"';
                }
                $queryStringParams = http_build_query(['articleNumber-in' => '[' . $params . ']']);
                break;
            default:
                unset($this->options['query']);
        }

        if ($queryStringParams) {
            $this->options['query'] = $queryStringParams;
        }
    }

    /**
     * @param                   $userData
     * @param SymfonyStyle|null $io
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    public function setCustomer($userData, SymfonyStyle $io = null): void
    {
        $this->addExtraConfigs($userData);
        $this->setBodyOptions($userData);
        $this->setQueryOptions(WeClappApi::CUSTOMER);
        try {
            $this->gClient->post(WeClappApi::CUSTOMER, $this->options);
        } catch (RequestException $e) {
            $result = $this->catchResult($e);
            if ($e->getCode() === 409) {
                if($io !== null) {
                    $io->success("Customer exists");
                    $io->warning("running update function");
                }
                $this->updateCustomer($userData, $io);
            } else {
                $this->showCommandErrors("Customer failed to add.", $io, $e);
                $this->pluginLogger->error('WeClapp: setCustomer', ['result' => $result, 'UserData' => $userData]);
            }
        }
    }

    /**
     * @param                   $userData
     * @param SymfonyStyle|null $io
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    private function updateCustomer($userData, SymfonyStyle $io = null): void
    {
        $this->setQueryOptions(WeClappApi::CUSTOMER, (string)$userData['customerNumber']);
        try {
            $response                           = $this->gClient->get(WeClappApi::CUSTOMER, $this->options);
            $result                             = $response->getBody()->getContents();
            $user                               = json_decode($result, 1, 512, JSON_THROW_ON_ERROR)['result'][0];
            $userData["useCustomsTariffNumber"] = true;
            $this->setBodyOptions($userData);
            $this->setQueryOptions();
            if ($io !== null) {
                $io->writeln("URL: " . WeClappApi::CUSTOMER_ID . $user['id']);
                $io->writeln($this->options, 3);
            }
            $this->gClient->put(WeClappApi::CUSTOMER_ID . $user['id'], $this->options);
            if ($io !== null) {
                $io->success("Customer was updated");
            }
        } catch (RequestException $e) {
            $result = $this->catchResult($e);
            $this->pluginLogger->error('WeClapp: updateCustomer', ['result' => $result, 'UserData' => $userData]);
            $this->showCommandErrors("Customer failed to updated.", $io, $e);
        }
    }

    /**
     * @param string            $msg
     * @param SymfonyStyle|null $io
     * @param null              $e
     *
     * @throws JsonException
     */
    private function showCommandErrors(string $msg, SymfonyStyle $io=null, $e=null): void
    {
        if ($io !== null) {
            $response = $e->getResponse();
            if($response === null){
                return;
            }
            $body = $response->getbody();
            $io->error($msg);
            $io->error(json_decode($body, true, 512, JSON_THROW_ON_ERROR)['error']);
        }
    }

    /**
     * @param                   $orderData
     * @param SymfonyStyle|null $io
     *
     * @throws GuzzleException
     * @throws JsonException
     */
    public function setOrder($orderData, SymfonyStyle $io = null): void
    {
        $this->addExtraConfigs($orderData, true);
        $this->addShippingCostsProduct($orderData);
        $this->setBodyOptions($orderData);

        if (!$this->orderExists($orderData)) {
            try {
                $this->gClient->post(WeClappApi::ORDER, $this->options);
            } catch (RequestException $e) {
                $result = $this->catchResult($e);
                $this->pluginLogger->error('WeClapp: setOrder', $result);
                $this->showCommandErrors("Failed to add Order", $io, $e);
            }
       }
    }

    /**
     * @param $orderData
     *

     */
    private function addShippingCostsProduct(&$orderData): void
    {
        try {
            $total        = $this->getTotalPrice($orderData);
            $supplierData = $this->getSupplierData();
            $max          = 0;
            if ($supplierData) {
                $max = $supplierData[0]['shippingfreefrom'];
            }

            if ($total < $max) {
                $orderData["orderItems"][] = [
                    "articleNumber"   => "001",
                    "quantity"        => 1,
                    "manualUnitPrice" => true,
                    "unitPrice"       => str_replace(",", ".", $this->configs['hrWeClappBaseShippingCosts']),
                ];
            }
        } catch (\Doctrine\DBAL\Driver\Exception $e) {
        } catch (\Doctrine\DBAL\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $orderData
     *
     * @return int
     */
    private function getTotalPrice($orderData): int
    {
        $total = 0;

        foreach ($orderData["orderItems"] as $order) {
            $total += $order["quantity"] * $order['unitPrice'];
        }

        return $total;
    }

    /**
     * @param $orderData
     *
     * @return bool
     * @throws GuzzleException|JsonException
     */
    private function orderExists($orderData): bool
    {
        $orderExists = false;
        $this->setQueryOptions(
            WeClappApi::ORDER,
            [
                'c' => (string)$orderData['customerNumber'],
                'o' => $orderData['orderNumberAtCustomer'],
            ]
        );

        try {
            $response    =
                $this->gClient->get(WeClappApi::ORDER, $this->options);
            $orderExists =
                !empty(json_decode($response->getBody()->getContents(), 1, 512, JSON_THROW_ON_ERROR)['result']);
        } catch (RequestException $e) {
            $result = $this->catchResult($e);
            $this->pluginLogger->error('WeClapp: preventDuplicateOrder', $result);
        }

        return $orderExists;
    }

    /**
     * @param       $data
     * @param false $isOrder
     */
    private function addExtraConfigs(&$data, bool $isOrder = false): void
    {
        $data['salesChannel'] = $this->configs['hrWeClappSalesChannel'];

        if ($isOrder) {
            $data['salesOrderPaymentType'] = 'PREPAYMENT';
        }
    }

    /**
     * @param $productData
     *
     * @return array|null
     * @throws \Doctrine\DBAL\Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function checkForProducts($productData): ?array
    {
        $products       = null;
        $pos            = 1;
        $vendorProducts = $this->getValidProducts();

        foreach ($productData as $product) {
            if (in_array($product['ordernumber'], $vendorProducts, true)) {
                $products[] = [
                    "articleNumber"  => $product['ordernumber'],
                    "positionNumber" => $pos++,
                    "quantity"       => $product['quantity'],
                    "unitPrice"      => str_replace(",", ".", $product['price']),
                ];
            }
        }

        return $products;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception
     */
    private function getValidProducts(): array
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select(['d.ordernumber', 'd.articleID'])
           ->from('s_articles', 'a')
           ->leftJoin('a', 's_articles_supplier', 's', 'a.supplierID = s.id')
           ->leftJoin('a', 's_articles_details', 'd', 'd.articleID = a.id')
           ->where($qb->expr()->like('s.name', ":hrWeClappVendor"))
           ->andWhere($qb->expr()->notLike('d.ordernumber', $qb->expr()->literal('HR%')))
           ->setParameter('hrWeClappVendor', $this->configs['hrWeClappVendor']);

        $results = $qb->execute()->fetchAllAssociative();

        $data = [];
        foreach ($results as $result) {
            $data[$result['articleID']] = $result['ordernumber'];
        }

        return $data;
    }

    /**
     * @return array
     * @throws \Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception
     */
    private function getSupplierData(): array
    {
        $qb = $this->connection->createQueryBuilder();

        $qb->select(['sa.shippingfreefrom'])
           ->from('s_articles_supplier', 's')
           ->leftJoin('s', 's_articles_supplier_attributes', 'sa', 's.id = sa.supplierID')
           ->where($qb->expr()->like('s.name', ":hrWeClappVendor"))
           ->setParameter('hrWeClappVendor', $this->configs['hrWeClappVendor']);

        return $this->processFetch($qb->execute()->fetchAllAssociative(), 'shippingfreefrom');
    }

    /**
     * @param        $data
     * @param string $assoc
     *
     * @return array
     */
    private function processFetch($data, string $assoc): array
    {
        $result = [];
        foreach ($data as $row) {
            $result[] = $row[$assoc];
        }

        return $result;
    }

    /**
     * @param RequestException $e
     *
     * @return array
     */
    private function catchResult(RequestException $e): array
    {
        $response = $e->getResponse();
        if ($response !== null) {
            $result = [
                'statusCode'   => $response->getStatusCode(),
                //'effectiveUrl' => $response->getEffectiveUrl(),
                'reasonPhrase' => $response->getReasonPhrase(),
                //'errorMessage' => $response-> $response->json()['error'],
            ];
        } else {
            $result = [
                'errorMessage' => $e->getMessage(),
                'statusCode'   => $e->getCode(),
            ];
        }

        return $result;
    }

    /**
     * @return array
     * @throws GuzzleException
     * @throws \Doctrine\DBAL\Exception|\Doctrine\DBAL\Driver\Exception|JsonException
     */
    public function getStock(): array
    {
        $product_ids = $this->getValidProducts();

        $this->setQueryOptions(WeClappApi::STOCK, $product_ids);

        try {
            $response  = $this->gClient->get(WeClappApi::STOCK);
            $stockData = json_decode($response->getBody()->getContents(), true)['result'];
        } catch (RequestException $e) {
            $result = $this->catchResult($e);
            $this->pluginLogger->error('WeClapp: getStock', $result);

            return $result;
        }

        $articles = [];
        foreach ($stockData as $item) {
            $articles[$item['articleNumber']] = $item['quantity'];
        }

        $stock = [];
        foreach ($product_ids as $article_id => $product_id) {
            $stock[$product_id]['id']    = $article_id;
            $stock[$product_id]['stock'] = array_key_exists($product_id, $articles) ? $articles[$product_id] : 0;
        }

        return $stock;
    }

    /**
     * @throws GuzzleException
     * @throws ConnectionException
     * @throws JsonException|\Doctrine\DBAL\Driver\Exception|\Doctrine\DBAL\Exception
     */
    public function updateStock(): void
    {
        $status['status'] = "error";

        if (isset($_GET['token']) && $_GET['token'] === $this->configs['hrWeClappAPIToken']) {
            $data = $this->getStock();

            foreach ($data as $ordernumber => $article) {
                try {
                    $this->deActivate($article);
                    $this->setStock($ordernumber, $article['stock']);
                } catch (Exception $e) {
                    $this->connection->rollBack();
                    $status['reason'] = "Error while adding entry to database.";
                }
            }
            $status['status'] = "success";
        } else {
            $status['reason'] = "Token missing or wrong.";
        }

        echo json_encode($status, JSON_THROW_ON_ERROR);
    }

    /**
     * @throws ConnectionException
     * @throws \Doctrine\DBAL\Exception
     */
    private function deActivate($article): void
    {
        $this->connection->beginTransaction();
        $this->connection->update(
            's_articles',
            [
                'active' => (int)$article['stock'] ? 1 : 0,
            ],
            [
                'id' => $article['id'],
            ]
        );
        $this->connection->commit();
    }

    /**
     * @param $ordernumber
     * @param $quantity
     *
     * @throws ConnectionException
     * @throws \Doctrine\DBAL\Exception
     */
    private function setStock($ordernumber, $quantity): void
    {
        $this->connection->beginTransaction();
        $this->connection->update(
            's_articles_details',
            [
                'instock' => $quantity,
            ],
            [
                'ordernumber' => $ordernumber,
            ]
        );
        $this->connection->commit();
    }

    /**
     * @param $data
     */
    public function debug($data): void
    {
        echo "<pre>";
        echo print_r($data, 1);
        echo "</pre>";
    }
}
