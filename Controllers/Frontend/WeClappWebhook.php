<?php

use GuzzleHttp\Exception\GuzzleException;
use HrWeClappConnector\Services\WeClappConnector;

class Shopware_Controllers_Frontend_WeClappWebhook extends Enlight_Controller_Action
{
    /**
     * @var WeClappConnector
     */
    private WeClappConnector $wcs;

    /**
     * Disable the template loading
     *
     * @throws Exception
     */
    public function preDispatch(): void
    {
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();
        $wcs = $this->get('hr_we_clapp_connector.service');
        if ($wcs) {
            $this->wcs = $wcs;
        }
    }

    public function indexAction(): void
    {
        try {
            $this->wcs->updateStock();
        } catch (Exception | \Doctrine\DBAL\Driver\Exception | GuzzleException $e) {
            echo $e->getMessage();
        }
    }

    private function debug($data){
        echo "<pre>";
        echo print_r($data, 1);
        echo "</pre>";
    }
}

