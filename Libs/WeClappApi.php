<?php

namespace HrWeClappConnector\Libs;

class WeClappApi
{
    public const API_URL     = 'weclapp.com/webapp/api/v1/';
    public const API_USER    = '*';
    public const ORDER       = 'salesOrder';
    public const CUSTOMER    = 'customer';
    public const CUSTOMER_ID = 'customer/id/';
    public const COMMENT     = 'comment';
    public const STOCK       = 'warehouseStock';

    /**
     * @param        $tenant
     * @param        $entity
     * @param string $params
     *
     * @return string
     */
    public static function url($tenant, $entity, string $params = ""): string
    {
        $url = "https://{$tenant}." . self::API_URL . "/{$entity}";
        if (!empty($params)) {
            $url .= "?{$params}";
        }

        return $url;
    }
}
