<?php

namespace HrWeClappConnector\Libs;

class DemoData
{
    public static array $demoConfig = [
        'hrWeClappAPIToken'        => '39cced17-319b-4212-9789-fd42ef67d521',
        'hrWeClappTenant'          => 'daxrokqhqaapmzl',
        'hrWeClappVendor'          => 'Berlin Organics',
        'hrWeClappSalesChannel'    => 'Health Rise',
        'hrWeClappWarehouseNAme'   => 'Hauptlager',
        'hrWeClappPaymentTypeName' => 'Vorkasse',
    ];

    public static array $demoOrder = [
        "customerNumber"        => "HR10000",
        "status"                => "ORDER_CONFIRMATION_PRINTED",
        "warehouseName"         => "Hauptlager",
        "salesChannel"          => "Health Rise",
        "orderNumberAtCustomer" => "TEST" . (string) random_int(10000, 50000),
        "orderItems"            => [
            [
                "articleNumber"  => "244",
                "positionNumber" => 1,
                "quantity"       => 2,
                "unitPrice"      => 10.00,
            ],
        ],
    ];

    public static array $demoCustomer = [
        "addresses"      => [
            [
                "city"         => "bad homburg v.dH",
                "countryCode"  => "DE",
                "primeAddress" => true,
                "street1"      => "Schaber Weg 28 a",
                "zipcode"      => "12345",
            ],
        ],
        "customerNumber"       => "HR10000",
        "email"                => "demo@demo.demo",
        "firstName"            => "Hans Peter",
        "lastName"             => "Lustig",
        "partyType"            => "PERSON",
        "phone"                => "069654321",
        "salutation"           => "MR",
        "salesChannel"         => "Health Rise",

        /** Required Parameters **/
        "blocked"              => false,
        "deliveryBlock"        => false,
        "insolvent"            => false,
        "insured"              => false,
        "invoiceBlock"         => false,
        "optIn"                => false,
        "optInLetter"          => false,
        "optInPhone"           => false,
        "optInSms"             => false,
        "responsibleUserFixed" => false,
        "salesPartner"         => false,
    ];
}
