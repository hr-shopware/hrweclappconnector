<?php

namespace HrWeClappConnector\Libs;

class ContentTypes
{
    public const XML  = 'application/x-www-form-urlencoded';
    public const JSON = 'application/json';
}
