<?php

namespace HrWeClappConnector\Commands;

use GuzzleHttp\Exception\GuzzleException;
use HrWeClappConnector\Libs\DemoData;
use HrWeClappConnector\Services\WeClappConnector;
use Shopware\Commands\ShopwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class WeClappTestCommand extends ShopwareCommand
{
    protected function configure(): void
    {
        $this->setName('hr:weclapp:test')
             ->setDescription('Test WeClappService')
             ->addOption('customer', 'c', InputOption::VALUE_NONE, "Test DemoCustomer add/update")
             ->addOption('order', 'o', InputOption::VALUE_NONE, "Test DemoOrder add")
             ->addOption('stock', 's', InputOption::VALUE_NONE, "Test Stock adjustment")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        /** @var WeClappConnector $wcs */
        $wcs = $this->container->get('hr_we_clapp_connector.service');
        $options = $input->getOptions();
            /*$testdata = [
            "addresses"      => [
                [
                    "city"         => "Musterstadt",
                    "countryCode"  => "DE",
                    "primeAddress" => true,
                    "street1"      => "Musterstraße",
                    "zipcode"      => "12345",
                ],
            ],
            "customerNumber" => "HR000000",
            "email"          => "dwayne.sharp@health-rise.de",
            "firstName"      => "Dwayne",
            "lastName"       => "Sharp",
            "partyType"      => "PERSON",
            "phone"          => "012345678",
            "salutation"     => "Herr",
            "salesChannel"   => "GROSS4"
        ];*/
        try{
            $opt = array_search("1", $options, 0);
            switch ($opt) {
                case "stock":
                    break;
                case "order":
                    $wcs->setOrder(DemoData::$demoOrder);
                    break;
                case "customer":
                    $wcs->setCustomer(DemoData::$demoCustomer, $io);
                    break;
                default:
                    $io->warning("Missing Option c = Customer, o = Order, s = Stock");
                    break;
            }
            echo print_r($opt,1)."\r\n";

            #$wcs->setCustomer(DemoData::$demoCustomer, $io);
        } catch (GuzzleException $e) {
            $io->error("GuzzleException: " . $e->getMessage());
        } catch (\JsonException $e) {
            $io->error("JsonException: " . $e->getMessage());
        }
    }
}