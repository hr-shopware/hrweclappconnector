<?php

namespace HrWeClappConnector\Subscriber;

use Enlight\Event\SubscriberInterface;
use Enlight_Controller_ActionEventArgs;
use Enlight_Controller_Request_Request;
use GuzzleHttp\Exception\GuzzleException;
use HrWeClappConnector\Services\WeClappConnector;
use Shopware_Controllers_Frontend_Checkout;

class OrderSubscriber implements SubscriberInterface
{
    private WeClappConnector $weClappService;

    public function __construct(WeClappConnector $weClappService)
    {
        $this->weClappService = $weClappService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'Enlight_Controller_Action_PostDispatch_Frontend_Checkout' => 'onPostDispatchFrontendCheckout',
        ];
    }

    /**
     * @throws GuzzleException
     */
    public function onPostDispatchFrontendCheckout(Enlight_Controller_ActionEventArgs $args): void
    {
        /** @var Shopware_Controllers_Frontend_Checkout $subject */
        $subject = $args->get('subject');
        /** @var Enlight_Controller_Request_Request $request */
        $request    = $subject->Request();
        $actionName = $request->getActionName();

        if ($subject->View() === null) {
            return;
        }

        $assignedData = $subject->View()->getAssign();
        $userData     = $assignedData['sUserData'];
        $user         = $userData['additional']['user'];
        $country      = $userData['additional']['country'];
        $basket       = $assignedData['sBasket']['content'];
        $address      = $userData['shippingaddress'];

        if (($actionName === 'finish') && ($products = $this->weClappService->checkForProducts($basket)) !== null) {
            $this->addCustomer(['user' => $user, 'address' => $address, 'country' => $country]);

            $this->addOrder(['user' => $user, 'products' => $products, 'orderNumber' => $assignedData['sOrderNumber']]);
        }
    }

    /**
     * @throws GuzzleException
     */
    private function addOrder($data): void
    {
        $orderData = [
            "customerNumber"        => "HR" . $data['user']['customernumber'],
            "orderNumberAtCustomer" => $data['orderNumber'],
            "status"                => "ORDER_CONFIRMATION_PRINTED",
            "warehouseName"         => "Hauptlager",
        ];

        $orderData["orderItems"] = $data['products'];

        $this->weClappService->setOrder($orderData);
    }

    /**
     * @throws GuzzleException
     */
    private function addCustomer($data): void
    {
        $userData = [
            "addresses"      => [
                [
                    "city"         => $data['address']['city'],
                    "countryCode"  => $data['country']['countryiso'],
                    "primeAddress" => true,
                    "street1"      => $data['address']['street'],
                    "zipcode"      => $data['address']['zipcode'],
                ],
            ],
            "customerNumber"       => "HR" . $data['user']['customernumber'],
            "email"                => $data['user']['email'],
            "firstName"            => $data['user']['firstname'],
            "lastName"             => $data['user']['lastname'],
            "partyType"            => "PERSON",
            "phone"                => $data['address']['phone'],
            "salutation"           => strtoupper($data['user']['salutation']),
            /** Required Parameters **/
            "blocked"              => false,
            "deliveryBlock"        => false,
            "insolvent"            => false,
            "insured"              => false,
            "invoiceBlock"         => false,
            "optIn"                => false,
            "optInLetter"          => false,
            "optInPhone"           => false,
            "optInSms"             => false,
            "responsibleUserFixed" => false,
            "salesPartner"         => false,
        ];

        $this->weClappService->setCustomer($userData);
    }
}
